<?php

$markers['cached'] = function($locator, $content) {
  return (!empty($content->cache) && !empty($content->cache['method']));
};

$markers['disabled'] = function($locator, $content) {
  return empty($content->shown);
};

$markers['incode'] = function($locator, $content) {
  return !is_numeric($content->pid);
};

$markers['indb'] = function($locator, $content) {
  return is_numeric($content->pid);
};

$markers['mini_panel'] = function($locator, $content) {
  return ($content->type == 'panels_mini');
};

$markers['view'] = function($locator, $content) {
  return ($content->type == 'views');
};

$markers['plain_banner'] = function($locator, $content) {
  /*
  if ($locator->hasMarker('disabled', $content) || $locator->hasParentMarker('disabled')) {
    return FALSE;
  }
  */

  $is_banner = FALSE;
  if ($content->type == 'banner') {
    $is_banner = FALSE;
  }
  elseif (strpos($content->admin_title, 'banner') !== FALSE) {
    $is_banner = TRUE;
  }
  elseif ($content->type != 'panels_mini' && strpos($content->subtype, 'banner') !== FALSE) {
    $is_banner = TRUE;
  }
  elseif ($content->type == 'custom' && strpos($content->configuration['body'], 'banner') !== FALSE) {
    $is_banner = TRUE;
  }
  static $uniquelinks = array();
  if (empty($uniquelinks[$content->_editpath])) {
    $uniquelinks[$content->_editpath] = TRUE;
  }
  return $is_banner;
};

$markers['plain_banner_unique'] = function($locator, $content) {
  if (!$locator->hasMarker('plain_banner', $content)) {
    return FALSE;
  }
  static $unique = array();
  if (empty($unique[$locator->uniqueContentKey($content)])) {
    $unique[$locator->uniqueContentKey($content)] = TRUE;
    return TRUE;
  }
  return FALSE;
};

$markers['cached_banner'] = function($locator, $content) {
  if (!$locator->hasMarker('plain_banner', $content)) {
    return FALSE;
  }

  if ($locator->hasMarker('cached', $content) || $locator->hasParentMarker('cached')) {
    return TRUE;
  }
  return FALSE;
};

$markers['disabled_banner'] = function($locator, $content) {
  return $locator->hasMarker('plain_banner', $content) && (
    $locator->hasMarker('disabled', $content) || $locator->hasParentMarker('disabled')
  );
};

$markers['unique_disabled_banner'] = function($locator, $content) {
  if (!$locator->hasMarker('disabled_banner', $content)) {
    return FALSE;
  }

  static $uniquepanes = array();
  if (empty($uniquepanes[$content->display->_name][$content->region][$content->pid])) {
    $uniquepanes[$content->display->_name][$content->region][$content->pid] = TRUE;
    return TRUE;
  }
  return FALSE;
};


$markers['unique_cached_banner'] = function($locator, $content) {
  if (!$locator->hasMarker('cached_banner', $content)) {
    return FALSE;
  }

  static $uniquepanes = array();
  if (empty($uniquepanes[$content->display->_name][$content->region][$content->pid])) {
    $uniquepanes[$content->display->_name][$content->region][$content->pid] = TRUE;
    return TRUE;
  }
  return FALSE;
};
